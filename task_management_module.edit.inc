<?php

/**
 * @file
 * Implement the Edit Task page.
 */

require_once "functions.php";

/**
 * Implements hook_form().
 */
function task_management_module_edit_form($form, &$form_state) {
  $form['#theme'] = 'task_management_module_edit_form';
  $id = arg(1);
  $rez = db_select('task_management_modules', 'nr')
      ->fields('nr')
      ->condition('nr.id', $id)
      ->execute()
      ->fetchAssoc();
  global $user;
  $ok = 0;
  foreach ($user->roles as $r) {
    if ($r == 'administrator') {
      $ok = 1;
    }
  }
  $form['to'] = array(
    '#type' => 'select',
    '#title' => t('Assigned to'),
    '#attributes' => $rez['assigned_by'] == $user->uid || $rez['assigned_to'] == $user->uid || $ok ? array() : array('disabled' => 'disabled'),
    '#default_value' => $rez['assigned_to'],
    '#options' => task_management_module_get_users(),
  );
  $form['by'] = array(
    '#type' => 'select',
    '#title' => t('Assigned by'),
    '#attributes' => $rez['assigned_by'] == ($user->uid || $rez['assigned_to'] == $user->uid || $ok) && 0 ? array() : array('disabled' => 'disabled'),
    '#default_value' => $rez['assigned_by'],
    '#options' => task_management_module_get_users(),
  );
  $form['importance'] = array(
    '#type' => 'select',
    '#title' => t('Importance'),
    '#default_value' => $rez['importance'],
    '#options' => task_management_module_importance(),
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array('disabled' => 'disabled'),
  );
  $form['system'] = array(
    '#type' => 'select',
    '#title' => t('System'),
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array('disabled' => 'disabled'),
    '#default_value' => $rez['project'],
    '#options' => task_management_module_sistem(),
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array('disabled' => 'disabled'),
    '#default_value' => $rez['type'],
    '#options' => task_management_module_type(),
  );
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok || $rez['assigned_to'] == $user->uid ? array() : array('disabled' => 'disabled'),
    '#default_value' => $rez['status'],
    '#options' => task_management_module_status(),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array("readonly" => "readonly"),
    '#title' => t('Title'),
    '#default_value' => $rez['title'],
    '#size' => '80',
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array("readonly" => "readonly"),
    '#description' => t('About task'),
    '#default_value' => $rez['description'],
    '#rows' => 10,
    '#cols' => 60,
    '#resizable' => TRUE,
  );
  $form['t_estimate'] = array(
    '#type' => 'textfield',
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array("readonly" => "readonly"),
    '#title' => t('Estimate time'),
    '#size' => '20',
    '#default_value' => $rez['t_estimate'],
    '#description' => t('hh:mm'),
  );
  $form['t_traking'] = array(
    '#type' => 'textfield',
    '#attributes' => array("readonly" => "readonly"),
    '#title' => t('Traking time'),
    '#default_value' => $rez['t_traking'],
    '#description' => t('hh:mm'),
    '#size' => '20',
  );
  $form['last_date'] = array(
    '#type' => 'textfield',
    '#attributes' => $rez['assigned_by'] == $user->uid || $ok ? array() : array("readonly" => "readonly"),
    '#description' => t('yyyy:mm:dd'),
    '#default_value' => $rez['last_date'],
    '#title' => t('Deadline'),
    '#size' => '20',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Updade Task'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_edit_form_submit'),
  );
  $header = array(
    'data0' => t('Filename'),
    'data1' => t('Options'),
  );

  if (isset($_GET['delete'])) {
    $da = db_select('m_files', 'nr')
        ->fields('nr')
        ->condition('nr.id', $_GET['delete'])
        ->execute()
        ->fetchAssoc();
    if ($ok || $user->uid == $da['user']) {
      $rez = db_delete('m_files')
          ->condition('id', $_GET['delete'])
          ->execute();
      $file = file_load($da['fid']);
      file_delete($file);

      db_insert('m_history')
          ->fields(array(
            'task' => arg(1),
            'field' => task_management_module_hs(7),
            'changes' => '',
            'user' => $user->uid,
            'time' => date('Y:m:d H:i:s'),
          ))
          ->execute();
    }
    else {
      drupal_set_message(t("You don't have right to delete this file!!"));
    }
    drupal_goto('task_management_module_edit/' . arg(1));
  }

  $rez = db_select('m_files', 'nr')
      ->fields('nr')
      ->condition('nr.task', $id)
      ->execute()
      ->fetchAll();
  $options = array();
  if ($rez) {
    foreach ($rez as $record) {
      $file = file_load($record->fid);
      $link1 = l(t('Delete'), 'task_management_module_edit/' . arg(1), array('query' => array('delete' => $record->id), 'attributes' => array('class' => array('about-link'))));
      $download = l(t("@file", array("@file" => $file->filename)), 'download/file/fid/' . $record->fid);

      $options[$record->id] = array(
        'data0' => $download,
        'data1' => $link1,
      );
    }
  }
  $form['table_files'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No files found'),
  );
  $form['file'] = array(
    '#name' => t('Upload reference'),
    '#type' => 'managed_file',
    '#description' => t('Pdf, doc, jpeg, png, doc, docx, pdf'),
    '#default_value' => variable_get('task_management_module_doc_file', ''),
    '#upload_location' => 'public://task_management_module_doc_file/',
    '#upload_validators' => array('file_validate_extensions' => array('jpeg jpg png docx doc pdf')/* , 'assets_validate_not_filename' => array('test.') */),
    '#progress_indicator' => 'bar',
    '#progress_message' => 'One moment while we save your file...',
  );
  $form['files'] = array(
    '#type' => 'submit',
    '#value' => t('Save file'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_fiels_form_submit'),
  );

  $header = array(
    'data8' => t('#'),
    'data0' => t('User'),
    'data1' => t('Date'),
    'data2' => t('Time'),
    'data3' => t('Text'),
    'data4' => t('Options'),
  );
  $rez = db_select('m_coments', 'nr')
      ->fields('nr')
      ->condition('nr.task', $id)
      ->execute()
      ->fetchAll();
  $options = array();
  if ($rez) {
    foreach ($rez as $record) {
      $link1 = l(t('Edit'), 'task_management_module_edit/' . arg(1), array('query' => array('edit' => $record->id), 'attributes' => array('class' => array('about-link'))));

      $options[$record->id] = array(
        'data8' => $record->id,
        'data0' => task_management_module_username($record->user),
        'data1' => $record->date,
        'data2' => $record->t_traking,
        'data3' => $record->text,
        'data4' => $link1,
      );
    }
  }
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No coments found'),
  );
  if (isset($_GET['edit'])) {
    $la = db_select('m_coments', 'nr')
        ->fields('nr')
        ->condition('nr.id', $_GET['edit'])
        ->execute()
        ->fetchAssoc();
  }
  $form['time'] = array(
    '#type' => 'textfield',
    '#title' => t('Time'),
    '#description' => t('hh:mm'),
    '#default_value' => isset($_GET['edit']) && ($ok || $user->uid == $la['user']) ? $la['t_traking'] : '',
    '#size' => '20',
  );

  $form['coment'] = array(
    '#title' => t('Comment'),
    '#type' => 'textarea',
    '#description' => t('About you did'),
    '#default_value' => isset($_GET['edit']) && ($ok || $user->uid == $la['user']) ? $la['text'] : '',
    '#rows' => 10,
    '#cols' => 60,
    '#resizable' => TRUE,
  );
  if (!isset($_GET['edit']) || !($ok || $user->uid == $la['user'])) {
    $form['coment_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add coment'),
      '#attributes' => array('class' => array('pay')),
      '#validate' => array('task_management_module_coment_form_validate'),
      '#submit' => array('task_management_module_coment_form_submit'),
    );
  }
  else {
    $form['coment_submit'] = array(
      '#type' => 'submit',
      '#value' => t('Edit coment'),
      '#attributes' => array('class' => array('pay')),
      '#validate' => array('task_management_module_coment_edit_form_validate'),
      '#submit' => array('task_management_module_coment_edit_form_submit'),
    );
  }

  $header = array(
    'data0' => t('User'),
    'data1' => t('Date'),
    'data2' => t('Field'),
    'data3' => t('Change'),
  );
  $rez = db_select('m_history', 'nr')
      ->fields('nr')
      ->condition('nr.task', $id)
      ->orderBy('time')
      ->execute()
      ->fetchAll();
  $options = array();
  if ($rez) {
    foreach ($rez as $record) {
      $options[$record->id] = array(
        'data0' => task_management_module_username($record->user),
        'data1' => $record->time,
        'data2' => $record->field,
        'data3' => $record->changes,
      );
    }
  }
  $form['table1'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No coments found'),
  );

  return $form;
}

/**
 * Implements update task's time.
 */
function task_management_module_update_traking($id) {
  $rez = db_select('m_coments', 'nr')
      ->fields('nr', array('t_traking'))
      ->condition('nr.task', $id)
      ->execute()
      ->fetchAll();
  $hour = 0;
  $min = 0;
  foreach ($rez as $r) {
    $d = explode(":", $r->t_traking);
    $hour += $d[0];
    $min += $d[1];
    if ($min >= 60) {
      $hour++;
      $min = $min - 60;
    }
  }
  db_update('task_management_modules')
      ->fields(array(
        't_traking' => $hour . ':' . ($min < 10 ? "0" . $min : $min),
      ))
      ->condition('id', $id)
      ->execute();
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_coment_edit_form_validate(&$form, &$form_state) {

  if ($form_state['values']['coment'] == '') {
    form_set_error($form_state['values']['coment'], t('Type Comment!!'));
  }

  $d = explode(":", $form_state['values']['time']);
  if (count($d) != 2 || !is_numeric($d[0]) || !is_numeric($d[1])) {
    form_set_error($form_state['values']['time'], t('Incorect format for Comment time!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_coment_edit_form_submit($form, $form_state) {
  if ($form_state['values']['time'] != '' && $form_state['values']['coment'] != '') {
    global $user;
    $id = arg(1);

    $id = db_update('m_coments')
        ->fields(array(
          'text' => $form_state['values']['coment'],
          't_traking' => $form_state['values']['time'],
          'date' => date("Y:m:d H:i:s"),
          'private' => 0,
          'task' => $id,
          'user' => $user->uid,
        ))
        ->condition('id', $_GET['edit'])
        ->execute();
    $item1 = arg(1);
    $item = $_GET['edit'];
    $id = db_insert('m_history')
        ->fields(array(
          'task' => $item1,
          'field' => task_management_module_hs(4) . ' #' . $item,
          'changes' => '',
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  task_management_module_update_traking(arg(1));
  drupal_goto('task_management_module_edit/' . arg(1));
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_fiels_form_submit($form, $form_state) {
  if ($form_state['values']['file'] == 0 || $form_state['values']['file'] == '') {
    return;
  }
  global $user;
  $file = file_load($form_state['values']['file']);
  $file->status = FILE_STATUS_PERMANENT;
  file_save($file);
  db_insert('m_files')
      ->fields(array(
        'task' => arg(1),
        'user' => $user->uid,
        'fid' => $file->fid,
      ))
      ->execute();
  $item = arg(1);
  db_insert('m_history')
      ->fields(array(
        'task' => $item,
        'field' => task_management_module_hs(6) . ' ' . $file->filename,
        'changes' => '',
        'user' => $user->uid,
        'time' => date('Y:m:d H:i:s'),
      ))
      ->execute();
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_edit_form_validate(&$form, &$form_state) {

  if ($form_state['values']['description'] == '') {
    form_set_error($form_state['values']['description'], t('Type description!!'));
  }
  if ($form_state['values']['title'] == '') {
    form_set_error($form_state['values']['title'], t('Type task"s tilte!!'));
  }
  if ($form_state['values']['system'] == 0) {
    form_set_error($form_state['values']['system'], t('Choose System is task about!!'));
  }
  if ($form_state['values']['importance'] == 0) {
    form_set_error($form_state['values']['system'], t('Choose importance of a task !!'));
  }
  if ($form_state['values']['type'] == 0) {
    form_set_error($form_state['values']['type'], t('Choose type of a task !!'));
  }
  if ($form_state['values']['to'] == 0) {
    form_set_error($form_state['values']['system'], t('Choose person is task assign to!!'));
  }

  $d = explode(":", $form_state['values']['last_date']);
  if (count($d) != 3 || !is_numeric($d[0]) || !is_numeric($d[1]) || !is_numeric($d[2])) {
    form_set_error($form_state['values']['last_date'], t('Incorect format for deadline!!'));
  }

  $d = explode(":", $form_state['values']['t_estimate']);
  if (count($d) != 2 || !is_numeric($d[0]) || !is_numeric($d[1])) {
    form_set_error($form_state['values']['t_estimate'], t('Incorect format for Estimate time!!'));
  }

  $d = explode(":", $form_state['values']['t_traking']);
  if (count($d) != 2 || !is_numeric($d[0]) || !is_numeric($d[1])) {
    form_set_error($form_state['values']['t_traking'], t('Incorect format for Traking time!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_edit_form_submit($form, $form_state) {
  global $user;
  $rez = db_select('task_management_modules', 'nr')
      ->fields('nr')
      ->condition('id', arg(1))
      ->execute()
      ->fetchAssoc();
  $item = arg(1);
  if ($form_state['values']['importance'] != $rez['importance']) {
    db_insert('m_history')
        ->fields(array(
          'task' => $item,
          'field' => task_management_module_hs(11),
          'changes' => task_management_module_get_priority($rez['importance']) . '=>' . task_management_module_get_priority($form_state['values']['importance']),
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  if ($form_state['values']['to'] != $rez['assigned_to']) {
    db_insert('m_history')
        ->fields(array(
          'task' => $item,
          'field' => task_management_module_hs(8),
          'changes' => task_management_module_username($rez['assigned_to']) . '=>' . task_management_module_username($form_state['values']['to']),
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  if ($form_state['values']['status'] != $rez['status']) {
    db_insert('m_history')
        ->fields(array(
          'task' => $item,
          'field' => task_management_module_hs(5),
          'changes' => task_management_module_get_status($rez['status']) . '=>' . task_management_module_get_status($form_state['values']['status']),
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  if ($form_state['values']['system'] != $rez['project']) {
    db_insert('m_history')
        ->fields(array(
          'task' => $item,
          'field' => task_management_module_hs(2),
          'changes' => $rez['project'] . '=>' . $form_state['values']['system'],
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  if ($form_state['values']['t_estimate'] != $rez['t_estimate']) {
    db_insert('m_history')
        ->fields(array(
          'task' => $item,
          'field' => task_management_module_hs(10),
          'changes' => $rez['t_estimate'] . '=>' . $form_state['values']['t_estimate'],
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  if ($form_state['values']['last_date'] != $rez['last_date']) {
    db_insert('m_history')
        ->fields(array(
          'task' => $item,
          'field' => task_management_module_hs(9),
          'changes' => $rez['last_date'] . '=>' . $form_state['values']['last_date'],
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  db_update('task_management_modules')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'description' => $form_state['values']['description'],
        'importance' => $form_state['values']['importance'],
        'project' => $form_state['values']['system'],
        'assigned_by' => $form_state['values']['to'] != $rez['assigned_to'] ? $user->uid : $rez['assigned_by'],
        'assigned_to' => $form_state['values']['to'],
        'last_date' => $form_state['values']['last_date'],
        't_estimate' => $form_state['values']['t_estimate'],
        't_traking' => $form_state['values']['t_traking'],
        'status' => $form_state['values']['status'],
        'type' => $form_state['values']['type'],
      ))
      ->condition('id', arg(1))
      ->execute();
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_coment_form_validate(&$form, &$form_state) {

  if ($form_state['values']['coment'] == '') {
    form_set_error($form_state['values']['coment'], t('Type Comment!!'));
  }

  $d = explode(":", $form_state['values']['time']);
  if (count($d) != 2 || !is_numeric($d[0]) || !is_numeric($d[1])) {
    form_set_error($form_state['values']['time'], t('Incorect format for Comment time!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_coment_form_submit($form, $form_state) {
  if ($form_state['values']['time'] != '' && $form_state['values']['coment'] != '') {
    global $user;
    $id = arg(1);

    db_insert('m_coments')
        ->fields(array(
          'text' => $form_state['values']['coment'],
          't_traking' => $form_state['values']['time'],
          'date' => date("Y:m:d H:i:s"),
          'private' => 0,
          'task' => $id,
          'user' => $user->uid,
        ))
        ->execute();
    $item1 = arg(1);
    $item = db_query('SELECT LAST_INSERT_ID()')->fetchField();
    db_insert('m_history')
        ->fields(array(
          'task' => $item1,
          'field' => task_management_module_hs(3) . ' #' . $item,
          'changes' => '',
          'user' => $user->uid,
          'time' => date('Y:m:d H:i:s'),
        ))
        ->execute();
  }
  task_management_module_update_traking(arg(1));
}
