
   
 * Introduction
 
 This is a module that manage the work flow in a team.
Possibilities:
- Adding projects
- Adding Statuses for task
- Adding Priorities for task
- Adding Task type
- Adding Task(Assigned to, deadline)
- Adding comments to a task(time, text)
- Adding files to a task
- Edit comments
- Delete files of a task
- Edit task(type,priorities,status)

 * Requirements
 One needs to install download_file module

 * Installation
 Module is installing from module panel.
 In database is creating some tables:
		m_projects
		m_importance
		task_management_modules
		m_status
		m_files
		m_coments
		m_type
		m_history

 * Configuration
 In Navigation list appear links that one can use in order to configure the
module:
- Adding projects
- Adding Statuses for task
- Adding Priorities for task
- Adding Task type
 In permission panel one can set who can edit the task elements: type, statuses,
 etc.
 
 * Troubleshooting
 If the links does not appear check if the permissions is set.
 * FAQ
 //not yet
 * Maintainers
 Current maintainers:
 * Mosnoi Ion - https://drupal.org/user/2295214
