<?php

/**
 * @file
 * Implement the status requirement.
 */

/**
 * Implements hook_form().
 */
function task_management_module_type_form($form, &$form_state) {
  $header = array(
    'data0' => t('Title'),
  );
  $options = array();
  $rez = db_select('m_type', 'nr')
      ->fields('nr', array('text', 'id'))
      ->execute()
      ->fetchAll();
  if ($rez) {
    foreach ($rez as $record) {
      $options[$record->id] = array(
        'data0' => $record->text,
      );
    }
  }

  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No type found'),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t("Title"),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_type_form_submit'),
  );
  $form['submit1'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_delete_type_form_submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_type_form_validate(&$form, &$form_state) {
  if ($form_state['values']['title'] == '') {
    form_set_error($form_state['values']['title'], t('Type status"s tilte!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_delete_type_form_submit($form, $form_state) {

  $i = 0;
  $id = $form_state['values']['table'];
  $i = 0;
  foreach ($id as $a) {
    if ($a != 0) {
      $i++;
      $p = $a;
    }
  }
  if ($i != 1) {
    drupal_set_message(t('Select just one row'));
  }
  else {
    db_delete('m_type')
        ->condition('id', $p)
        ->execute();
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_type_form_submit($form, $form_state) {

  drupal_set_message(t("It's inserted!!"));
  db_insert('m_type')
      ->fields(array(
        'text' => $form_state['values']['title'],
      ))
      ->execute();
}
