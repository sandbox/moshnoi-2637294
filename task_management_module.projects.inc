<?php

/**
 * @file
 * Implement the projects requierement.
 */

/**
 * Implements hook_form().
 */
function task_management_module_projects_form($form, &$form_state) {
  $header = array(
    'data0' => t('Title'),
    'data1' => t('Description'),
  );
  $options = array();
  $rez = db_select('m_projects', 'nr')
      ->fields('nr', array('title', 'description', 'id'))
      ->execute()
      ->fetchAll();
  if ($rez) {
    foreach ($rez as $record) {
      $options[$record->id] = array(
        'data0' => $record->title,
        'data1' => $record->description,
      );
    }
  }

  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No projects found'),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t("Title"),
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#rows' => 5,
    '#cols' => 20,
    '#resizable' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_projects_form_submit'),
  );
  $form['submit1'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_delete_projects_form_submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_projects_form_validate(&$form, &$form_state) {
  if ($form_state['values']['title'] == '') {
    form_set_error($form_state['values']['title'], t('Type task"s tilte!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_delete_projects_form_submit($form, $form_state) {

  $i = 0;
  $id = $form_state['values']['table'];
  $i = 0;
  foreach ($id as $a) {
    if ($a != 0) {
      $i++;
      $p = $a;
    }
  }
  if ($i != 1) {
    drupal_set_message(t('Select just one row'));
  }
  else {
    $rez = db_select('task_management_modules', 'nr')
        ->fields('nr', array('id'))
        ->condition('nr.project', $p)
        ->execute()
        ->fetchField();
    if ($rez) {
      drupal_set_message(t("Can't delete that project because you have task about its!!!"));
    }
    else {
      db_delete('m_projects')
          ->condition('id', $p)
          ->execute();
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_projects_form_submit($form, $form_state) {
  drupal_set_message(t("It's Inserted!!"));
  db_insert('m_projects')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'description' => $form_state['values']['description'],
      ))
      ->execute();
}
