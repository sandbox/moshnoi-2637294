<?php

/**
 * @file
 * Implement the functions that Will be used in other files.
 */

/**
 * Return the array of importance.
 */
function task_management_module_importance() {
  $op = array(0 => 'Any');
  $rez = db_select('m_importance', 'nr')
      ->fields('nr', array('id', 'text'))
      ->execute()
      ->fetchAll();

  $op = array(0 => 'Any');
  if ($rez) {
    foreach ($rez as $record) {
      $op[$record->id] = $record->text;
    }
  }
  return $op;
}

/**
 * Return the array of priority.
 */
function task_management_module_get_priority($id) {
  $rez = db_select('m_importance', 'nr')
      ->fields('nr', array('text'))
      ->condition('nr.id', $id)
      ->execute()
      ->fetchField();
  return $rez;
}

/**
 * Return the array of systems.
 */
function task_management_module_sistem() {
  $op = array(0 => 'Any');
  $rez = db_select('m_projects', 'nr')
      ->fields('nr', array('title', 'description', 'id'))
      ->execute()
      ->fetchAll();
  if ($rez) {
    foreach ($rez as $record) {
      $op[$record->id] = $record->title;
    }
  }
  return $op;
}

/**
 * Return the array of comments for changes.
 */
function task_management_module_hs($a) {
  $hystory = array(
    1 => 'Task created',
    2 => 'Organization',
    3 => 'Note added',
    4 => 'Note edited',
    5 => "Status",
    6 => 'File added',
    7 => 'File deleted',
    8 => 'Assigned',
    9 => 'Deadline',
    10 => "Estimate time",
    11 => 'Priority',
  );
  return $hystory[$a];
}

/**
 * Return the array of statuses.
 */
function task_management_module_status() {
  $op = array();
  $rez = db_select('m_status', 'nr')
      ->fields('nr', array('text', 'id'))
      ->execute()
      ->fetchAll();
  if ($rez) {
    foreach ($rez as $record) {
      $op[$record->id] = $record->text;
    }
  }
  return $op;
}

/**
 * Return the array of statuses rez.
 */
function task_management_module_get_status($id) {

  $rez = db_select('m_status', 'nr')
      ->fields('nr', array('text'))
      ->condition('nr.id', $id)
      ->execute()
      ->fetchField();
  return $rez;
}

/**
 * Return the array of type.
 */
function task_management_module_type() {
  $op = array(0 => 'Any');
  $rez = db_select('m_type', 'nr')
      ->fields('nr', array('text', 'id'))
      ->execute()
      ->fetchAll();
  if ($rez) {
    foreach ($rez as $record) {
      $op[$record->id] = $record->text;
    }
  }
  return $op;
}

/**
 * Return the array of user array.
 */
function task_management_module_get_users() {
  $rez = db_select('users', 'nr')
      ->fields('nr', array('uid', 'name'))
      ->execute()
      ->fetchAll();

  $op = array(0 => 'Any');
  if ($rez) {
    foreach ($rez as $record) {
      $op[$record->uid] = $record->name;
    }
  }
  return $op;
}

/**
 * Return the array of user name.
 */
function task_management_module_username($uid) {
  $user = user_load($uid);
  return $user->name;
}
