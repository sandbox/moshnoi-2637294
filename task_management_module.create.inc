<?php

/**
 * @file
 * Implement the Account page.
 */

require_once "functions.php";

/**
 * Implements hook_form().
 */
function task_management_module_create_form($form, &$form_state) {
  $form['#theme'] = 'task_management_module_create_form';
  $form['to'] = array(
    '#type' => 'select',
    '#title' => t('Assign to'),
    '#options' => task_management_module_get_users(),
  );
  $form['importance'] = array(
    '#type' => 'select',
    '#title' => t('Importance'),
    '#options' => task_management_module_importance(),
  );
  $form['system'] = array(
    '#type' => 'select',
    '#title' => t('System'),
    '#options' => task_management_module_sistem(),
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => task_management_module_type(),
  );
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => task_management_module_status(),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#size' => '80',
  );
  $form['t_estimate'] = array(
    '#type' => 'textfield',
    '#title' => t('Estimate time'),
    '#size' => '20',
    '#description' => t('hh:mm'),
  );
  $form['t_traking'] = array(
    '#type' => 'textfield',
    '#title' => t('Traking time'),
    '#description' => t('hh:mm'),
    '#size' => '20',
    '#attributes' => array("readonly" => "readonly"),
    '#default_value' => t("00:00"),
  );
  $form['last_date'] = array(
    '#type' => 'textfield',
    '#description' => t('yyyy:mm:dd'),
    '#title' => t('Deadline'),
    '#size' => '20',
  );
  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#description' => t('About task'),
    '#default_value' => 'Write some text ...',
    '#rows' => 10,
    '#cols' => 60,
    '#resizable' => TRUE,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Task'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_create_form_submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_create_form_validate(&$form, &$form_state) {
  if ($form_state['values']['description'] == '') {
    form_set_error($form_state['values']['description'], t('Type description!!'));
  }
  if ($form_state['values']['title'] == '') {
    form_set_error($form_state['values']['title'], t('Type task"s tilte!!'));
  }
  if ($form_state['values']['system'] == 0) {
    form_set_error($form_state['values']['system'], t('Choose System is task about!!'));
  }
  if ($form_state['values']['importance'] == 0) {
    form_set_error($form_state['values']['system'], t('Choose importance of a task !!'));
  }
  if ($form_state['values']['type'] == 0) {
    form_set_error($form_state['values']['type'], t('Choose type of a task !!'));
  }
  if ($form_state['values']['to'] == 0) {
    form_set_error($form_state['values']['system'], t('Choose person is task assign to!!'));
  }

  $d = explode(":", $form_state['values']['last_date']);
  if (count($d) != 3 || !is_numeric($d[0]) || !is_numeric($d[1]) || !is_numeric($d[2])) {
    form_set_error($form_state['values']['last_date'], t('Incorect format for deadline!!'));
  }
  $d = explode(":", $form_state['values']['t_estimate']);
  if (count($d) != 2 || !is_numeric($d[0]) || !is_numeric($d[1])) {
    form_set_error($form_state['values']['t_estimate'], t('Incorect format for Estimate time!!'));
  }
  $d = explode(":", $form_state['values']['t_traking']);
  if (count($d) != 2 || !is_numeric($d[0]) || !is_numeric($d[1])) {
    form_set_error($form_state['values']['t_traking'], t('Incorect format for Traking time!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_create_form_submit($form, $form_state) {
  global $user;

  $id = db_insert('task_management_modules')
      ->fields(array(
        'title' => $form_state['values']['title'],
        'description' => $form_state['values']['description'],
        'importance' => $form_state['values']['importance'],
        'project' => $form_state['values']['system'],
        'assigned_by' => $user->uid,
        'assigned_to' => $form_state['values']['to'],
        'type' => $form_state['values']['type'],
        'last_date' => $form_state['values']['last_date'],
        't_estimate' => $form_state['values']['t_estimate'],
        't_traking' => $form_state['values']['t_traking'],
        'status' => $form_state['values']['status'],
      ))
      ->execute();
  $item = db_query('SELECT LAST_INSERT_ID()')->fetchField();
  $id = db_insert('m_history')
      ->fields(array(
        'task' => $item,
        'field' => task_management_module_hs(1),
        'changes' => '',
        'user' => $user->uid,
        'time' => date('Y:m:d H:i:s'),
      ))
      ->execute();
  $id = db_insert('m_history')
      ->fields(array(
        'task' => $item,
        'field' => task_management_module_hs(8),
        'changes' => task_management_module_username($form_state['values']['to']),
        'user' => $user->uid,
        'time' => date('Y:m:d H:i:s'),
      ))
      ->execute();
  $id = db_insert('m_history')
      ->fields(array(
        'task' => $item,
        'field' => task_management_module_hs(2),
        'changes' => $form_state['values']['system'],
        'user' => $user->uid,
        'time' => date('Y:m:d H:i:s'),
      ))
      ->execute();
  $id = db_insert('m_history')
      ->fields(array(
        'task' => $item,
        'field' => task_management_module_hs(10),
        'changes' => $form_state['values']['t_estimate'],
        'user' => $user->uid,
        'time' => date('Y:m:d H:i:s'),
      ))
      ->execute();
  $id = db_insert('m_history')
      ->fields(array(
        'task' => $item,
        'field' => task_management_module_hs(9),
        'changes' => $form_state['values']['last_date'],
        'user' => $user->uid,
        'time' => date('Y:m:d H:i:s'),
      ))
      ->execute();
  if ($id) {
    drupal_set_message(t("New task is made!!"));
  }
  drupal_goto('/task_management_module_acount', array(
    'query' => array(),
    ));
}
