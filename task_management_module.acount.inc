<?php

/**
 * @file
 * Implement the Account page.
 */

/**
 * A string containing the Account HTML output.
 */
function task_management_module_acount() {
  $out = '';
  global $user;
  $out .= t('<h2>Assigned to me tasks</h2>');

  $q = db_select('task_management_modules', 'nr');
  $q->join('m_status', 'u', 'nr.status = u.id');
  $q->join('m_projects', 't', 'nr.project = t.id');
  $q->join('m_type', 'v', 'nr.type = v.id');
  $q->join('m_history', 'h', 'nr.id = h.task');
  $q->fields('nr', array('importance', 'project', 'id', 'title', 'status'));
  $q->addField('t', 'title', 'name');
  $q->addField('v', 'text', 'text');
  $q->addExpression('MAX(time)', 'time');
  $q->condition('nr.assigned_to', $user->uid);
  $q->condition('u.solved', 0);
  $q->orderBy('time', 'DESC');
  $q->groupBy('nr.id');
  $rez = $q->execute()->fetchAll();

  $out .= t('<ul>');
  if ($rez) {
    foreach ($rez as $r) {
      $link1 = l(t('#@id @title', array('@id' => $r->id, '@title' => $r->title)), 'task_management_module_edit/' . $r->id, array('query' => array('type' => '2'), 'attributes' => array('class' => array('about-link'))));
      $out .= t('<li>') . $link1 . t('<b>[ @name ] </b>@text @time </li>',
          array("@name" => $r->name, '@text' => $r->text, '@time' => $r->time));
    }
  }
  else {
    $out .= t("<li>No found tasks</li>");
  }

  $out .= t('</ul>');
  $out .= t('<h2>Reported by tasks</h2>');

  $q = db_select('task_management_modules', 'nr');
  $q->join('m_status', 'u', 'nr.status = u.id');
  $q->join('m_projects', 't', 'nr.project = t.id');
  $q->join('m_type', 'v', 'nr.type = v.id');
  $q->join('m_history', 'h', 'nr.id = h.task');
  $q->fields('nr', array('importance', 'project', 'id', 'title', 'status'));
  $q->addField('t', 'title', 'name');
  $q->addField('v', 'text', 'text');
  $q->addExpression('MAX(time)', 'time');
  $q->condition('nr.assigned_by', $user->uid);
  $q->orderBy('time', 'DESC');
  $q->groupBy('nr.id');
  $rez = $q->execute()->fetchAll();

  $out .= t('<ul>');
  if ($rez) {
    foreach ($rez as $r) {
      $link1 = l(t('#@id @title', array('@id' => $r->id, '@title' => $r->title)), 'task_management_module_edit/' . $r->id, array('query' => array('type' => '2'), 'attributes' => array('class' => array('about-link'))));
      $out .= t('<li>') . $link1 . t('<b>[ @name ] </b>@text @time </li>',
          array("@name" => $r->name, '@text' => $r->text, '@time' => $r->time));
    }
  }
  else {
    $out .= t("<li>No found tasks</li>");
  }
  $out .= t('</ul>');

  $out .= t('<h2>Solved tasks</h2>');

  $q = db_select('task_management_modules', 'nr');
  $q->join('m_status', 'u', 'nr.status = u.id');
  $q->join('m_projects', 't', 'nr.project = t.id');
  $q->join('m_type', 'v', 'nr.type = v.id');
  $q->join('m_history', 'h', 'nr.id = h.task');
  $q->fields('nr', array('importance', 'project', 'id', 'title', 'status'));
  $q->addField('t', 'title', 'name');
  $q->addField('v', 'text', 'text');
  $q->addExpression('MAX(time)', 'time');
  $q->condition('nr.assigned_to', $user->uid);
  $q->condition('u.solved', 1);
  $q->orderBy('time', 'DESC');
  $q->groupBy('nr.id');
  $rez = $q->execute()->fetchAll();

  $out .= t('<ul>');
  if ($rez) {
    foreach ($rez as $r) {
      $link1 = l(t('#@id @title', array('@id' => $r->id, '@title' => $r->title)), 'task_management_module_edit/' . $r->id, array('query' => array('type' => '2'), 'attributes' => array('class' => array('about-link'))));
      $out .= t('<li>') . $link1 . t('<b>[ @name ] </b>@text @time </li>',
          array("@name" => $r->name, '@text' => $r->text, '@time' => $r->time));
    }
  }
  else {
    $out .= t("<li>No found tasks</li>");
  }
  $out .= t('</ul>');
  return $out;
}
