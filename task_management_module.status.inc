<?php

/**
 * @file
 * Implement the status requirement.
 */

/**
 * Implements hook_form().
 */
function task_management_module_status_form($form, &$form_state) {
  $header = array(
    'data0' => t('Title'),
    'data1' => t('Solved'),
  );
  $options = array();
  $rez = db_select('m_status', 'nr')
      ->fields('nr', array('text', 'id', 'solved'))
      ->execute()
      ->fetchAll();
  if ($rez) {
    foreach ($rez as $record) {
      $options[$record->id] = array(
        'data0' => $record->text,
        'data1' => $record->solved,
      );
    }
  }

  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No status found'),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t("Title"),
  );
  $form['solved'] = array(
    '#type' => 'textfield',
    '#size' => '50',
    '#title' => t("Solved(1/0)"),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_status_form_submit'),
  );
  $form['submit1'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array('class' => array('pay')),
    '#submit' => array('task_management_module_delete_status_form_submit'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function task_management_module_status_form_validate(&$form, &$form_state) {
  if ($form_state['values']['title'] == '') {
    form_set_error($form_state['values']['title'], t('Type task"s tilte!!'));
  }
  if ($form_state['values']['solved'] == '') {
    form_set_error($form_state['values']['solved'], t('Type status"s meaning(0/1)!!'));
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_delete_status_form_submit($form, $form_state) {

  $i = 0;
  $id = $form_state['values']['table'];
  $i = 0;
  foreach ($id as $a) {
    if ($a != 0) {
      $i++;
      $p = $a;
    }
  }
  if ($i != 1) {
    drupal_set_message(t('Select just one row'));
  }
  else {

    db_delete('m_status')
        ->condition('id', $p)
        ->execute();
  }
}

/**
 * Implements hook_form_submit().
 */
function task_management_module_status_form_submit($form, $form_state) {

  drupal_set_message(t("It's inserted!!"));
  db_insert('m_status')
      ->fields(array(
        'text' => $form_state['values']['title'],
        'solved' => $form_state['values']['solved'],
      ))
      ->execute();
}
